package com.example.maps.geofencing

import android.Manifest
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity(), OnMapReadyCallback,
    GoogleMap.OnMyLocationClickListener, GoogleMap.OnMyLocationButtonClickListener {

    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            for (location in locationResult.locations) {
                val newLocation = LatLng(location.latitude, location.longitude)
                map.moveCamera(CameraUpdateFactory.newLatLng(newLocation))
            }
        }
    }

    private val geocoder by lazy { Geocoder(applicationContext, Locale.getDefault()) }

    private lateinit var geofencingClient: GeofencingClient
    private val geofencePendingIntent: PendingIntent by lazy {
        val intent = Intent(this, GeofenceBroadcastReceiver::class.java)
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        checkPermissions()

        findViewById<CheckBox>(R.id.cbFollow).setOnCheckedChangeListener { buttonView, isChecked ->
            followCamera(isChecked)
        }

        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, BuildConfig.MAPS_API_KEY)
        }
    }

    @AfterPermissionGranted(RC_LOCATION)
    private fun checkPermissions() {
        val perms = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
        )

        if (EasyPermissions.hasPermissions(this, *perms)) {
            if (this::map.isInitialized) {
                enableMyLocationButton()
            }
            setupGeoFencing()
        } else {
            EasyPermissions.requestPermissions(
                this,
                "PERMISSIONS RATIONALE",
                RC_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
            )
        }
    }

    private fun followCamera(follow: Boolean) {
        if (follow) {
            val locationRequest = LocationRequest.create()?.apply {
                interval = 10000
                fastestInterval = 5000
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            }

            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.getMainLooper()
            )
        } else {
            fusedLocationClient.removeLocationUpdates(locationCallback)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        map.uiSettings.isCompassEnabled = true
        map.uiSettings.isZoomControlsEnabled = true

        val perms = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        if (EasyPermissions.hasPermissions(this, *perms)) {
            enableMyLocationButton()
        }

        fusedLocationClient.lastLocation.addOnSuccessListener { locationResult ->
            locationResult?.let { location ->
                val latLng = LatLng(location.latitude, location.longitude)
                map.moveCamera(CameraUpdateFactory.newLatLng(latLng))
                map.moveCamera(CameraUpdateFactory.zoomTo(14.0f))
            }
        }

        map.addCircle(
            CircleOptions()
                .center(GEOFENCE_CENTER)
                .radius(GEOFENCE_CIRCLE_RADIUS.toDouble())
                .fillColor(Color.argb(120, 0, 200, 50))
        )

        setupGeocoding()
        setupReverseGeoCoding()
    }

    private fun setupGeocoding() {
        findViewById<Button>(R.id.btnGeocoding).setOnClickListener {
            val addressName = findViewById<EditText>(R.id.etGeocoding).text.toString()
            if (addressName.isNotEmpty()) {
                val addressList = geocoder.getFromLocationName(addressName, 8)
                if (addressList.isNotEmpty()) {
                    val latLng = LatLng(addressList.first().latitude, addressList.first().longitude)
                    map.moveCamera(CameraUpdateFactory.newLatLng(latLng))
                }
            }
        }
    }

    private fun setupReverseGeoCoding() {
        map.setOnMapClickListener { location ->
            var addressString = ""

            // Reverse-Geocoding starts
            try {
                val addressList: List<Address> =
                    geocoder.getFromLocation(location.latitude, location.longitude, 1)

                // use your lat, long value here
                if (addressList.isNotEmpty()) {
                    val address = addressList[0]
                    val sb = StringBuilder()
                    for (i in 0 until address.maxAddressLineIndex) {
                        sb.append(address.getAddressLine(i)).append("\n")
                    }

                    // Various Parameters of an Address are appended
                    // to generate a complete Address
                    with(address) {
                        premises?.let { sb.append(it).append(", ") }
                        subAdminArea?.let { sb.append(it).append(", ") }
                        subThoroughfare?.let { sb.append(it).append(", ") }
                        thoroughfare?.let { sb.append(it).append("\n") }
                        locality?.let { sb.append(it).append(", ") }
                        adminArea?.let { sb.append(it).append(", ") }
                        countryName?.let { sb.append(it).append(", ") }
                        postalCode?.let { sb.append(it).append(", ") }
                    }

                    addressString = sb.toString()
                }
            } catch (e: IOException) {
                Toast.makeText(applicationContext, "Unable connect to Geocoder", Toast.LENGTH_LONG)
                    .show()
            }

            Toast.makeText(
                this,
                "Lat: ${location.latitude} \nLng: ${location.longitude} \nAddress: $addressString",
                Toast.LENGTH_SHORT
            ).show()
        }
    }


    private fun setupGeoFencing() {
        geofencingClient = LocationServices.getGeofencingClient(this)
        val geofenceList = createGeofenceList()
        val geofencingRequest = getGeofencingRequest(geofenceList)
        geofencingClient.addGeofences(geofencingRequest, geofencePendingIntent)?.run {
            addOnSuccessListener {
                Log.d(TAG, "setupGeoFencing: ADD SUCCESSFULLY")
            }
            addOnFailureListener {
                Log.d(TAG, "setupGeoFencing: ADD FAILURE: ${it}")
            }
        }
    }

    private fun createGeofenceList(): List<Geofence> = listOf(
        Geofence.Builder()
            .setCircularRegion(
                GEOFENCE_CENTER.latitude,
                GEOFENCE_CENTER.longitude,
                GEOFENCE_CIRCLE_RADIUS
            )
            .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER or Geofence.GEOFENCE_TRANSITION_EXIT)
            .setRequestId(GEOFENCE_REQUEST_ID)
            .setExpirationDuration(GEOFENCE_EXPIRATION_IN_MILLISECONDS)
            .build()
    )

    private fun getGeofencingRequest(geofenceList: List<Geofence>): GeofencingRequest =
        GeofencingRequest.Builder().apply {
            setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
            addGeofences(geofenceList)
        }.build()


    private fun enableMyLocationButton() {
        // Enable/disable myLocation layer
        map.isMyLocationEnabled = true
        // Enable/disable myLocation button
        map.uiSettings.isMyLocationButtonEnabled = true

        map.setOnMyLocationButtonClickListener(this)
        map.setOnMyLocationClickListener(this)
    }

    override fun onMyLocationButtonClick(): Boolean {
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false
    }

    override fun onMyLocationClick(location: Location) {

    }

    companion object {
        val TAG: String = MainActivity::class.java.simpleName
        const val RC_LOCATION = 5012

        const val GEOFENCE_EXPIRATION_IN_MILLISECONDS = 100_000L
        const val GEOFENCE_REQUEST_ID = "1001"
        val GEOFENCE_CENTER = LatLng(37.8191, -122.4785)
        const val GEOFENCE_CIRCLE_RADIUS = 1000f
    }
}