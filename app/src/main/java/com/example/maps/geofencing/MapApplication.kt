package com.example.maps.geofencing

import android.app.Application
import android.util.Log
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapsSdkInitializedCallback

class MapApplication: Application(), OnMapsSdkInitializedCallback {

    override fun onCreate() {
        super.onCreate()
        // Use the latest renderer
        MapsInitializer.initialize(this, MapsInitializer.Renderer.LATEST, this)
    }

    override fun onMapsSdkInitialized(renderer: MapsInitializer.Renderer) {
        when (renderer) {
            MapsInitializer.Renderer.LATEST -> Log.d(TAG, "Renderer is latest")
            MapsInitializer.Renderer.LEGACY -> Log.d(TAG, "Renderer is legacy")
        }
    }

    companion object {
        val TAG: String = MapApplication::class.java.simpleName
    }
}