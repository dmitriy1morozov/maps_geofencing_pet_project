package com.example.maps.geofencing

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofenceStatusCodes
import com.google.android.gms.location.GeofencingEvent

class GeofenceBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent) {
        val geofencingEvent = GeofencingEvent.fromIntent(intent)
        if (geofencingEvent.hasError()) {
            val errorMessage = GeofenceStatusCodes
                .getStatusCodeString(geofencingEvent.errorCode)
            Log.e(TAG, errorMessage)
            return
        }

        when (geofencingEvent.geofenceTransition) {
            Geofence.GEOFENCE_TRANSITION_ENTER ->
                Log.d(TAG, "Geofence Enter")
//                Toast.makeText(context, "Enter", Toast.LENGTH_SHORT).show()
            Geofence.GEOFENCE_TRANSITION_EXIT ->
                Log.d(TAG, "Geofence Exit")
//                Toast.makeText(context, "Exit", Toast.LENGTH_SHORT).show()
            else -> Toast.makeText(context, "Geofence Invalid geofence type", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        val TAG: String = GeofenceBroadcastReceiver::class.java.simpleName
    }
}
